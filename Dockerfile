FROM golang:1.14.4-buster
RUN apt-get update && \
    apt-get -y install rsync apt-utils zip && \
    apt-get -y remove apt-utils && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt/lists/*
